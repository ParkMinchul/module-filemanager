name := "FileManager"

version := "1.0"

scalaVersion := "2.11.6"


libraryDependencies ++= {
  val AkkaVersion = "2.3.11"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % AkkaVersion,
    "org.scalatest" %% "scalatest" % "2.2.4" % "test",
    "commons-io" % "commons-io" % "2.4"
  )
}