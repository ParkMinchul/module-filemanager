package com.ktzsoft.FileManager

import java.io._

import org.apache.commons.io.FileUtils

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
 * Created by ktz on 15. 6. 9.
 */
trait LocalFileManager extends FileManger[File]{
  def CopyFile(fileFrom: File, fileTo: File): Future[String] = Future[String] {
    if (fileFrom.exists()) {
      new FileOutputStream(fileTo) getChannel() transferFrom(new FileInputStream(fileFrom) getChannel, 0, Long.MaxValue)
      "File copy complete"
    } else {
      throw new FileNotFoundException("File Not Found")
    }
  }

  def MoveFile(fileFrom : File, fileTo : File) : Future[String] = Future[String]{
    val result = CopyFile(fileFrom, fileTo)
    result.onComplete{
      case Success(result : String) =>
        DeleteFile(fileFrom)
      case Failure(e : Throwable) =>
        throw e
    }
    s"Success To Move File $fileFrom"
  }

  def ReadFile(fileToRead: File): Future[FileInputStream] = Future[FileInputStream]{
      new FileInputStream(fileToRead)
    }

  def WriteFile(ByteToWrite: Array[Byte], fileToWrite: File): Future[String] = Future[String]{
    val writer = new PrintWriter(fileToWrite)
    writer.write(ByteToWrite map (_.toChar))
    "Write File Complete"
  }

  def DeleteFile(fileToDelete: File): Future[String] = Future[String] {
    fileToDelete.delete()
    "File Delete Complete"
  }

  def GetFileList(filePath : String) : List[File] = {
    val these = new File(filePath).listFiles.toList
    these ++ these.filter(_.isDirectory).flatMap(a=>GetFileList(a.getAbsolutePath)).toList
  }

  def CheckIfSame(fileFrom : File, fileTo : File) : Future[Boolean] = Future[Boolean] {
    FileUtils.contentEquals(fileFrom, fileTo)
  }
}