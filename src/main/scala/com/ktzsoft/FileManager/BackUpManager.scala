package com.ktzsoft.FileManager

import java.io.File

import akka.actor.Actor

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by ktz on 15. 6. 16.
 */

case class BackUp(fileToBackUp : File)

trait BackUpManager extends LocalFileManager{

  def CompareFiles(BackupFrom : File, BackupTo : File) : Future[Map[File, Boolean]] = Future[Map[File, Future[Boolean]]] {
    val backupFromList = GetFileList(BackupFrom.getAbsolutePath)
    (for{
      file <- backupFromList
      if(!file.isDirectory)
    } yield (file -> CheckIfSame(file, new File(GetTargetFilePath(BackupFrom.getAbsolutePath, file.getAbsolutePath, BackupTo.getAbsolutePath))))).toMap
  }.map(elem => elem.map(e => e._1 -> Await.result(e._2, 10 second)))

  def GetTargetFilePath(fileOriginDirectory : String, fileOriginPath : String, targetDirectory : String) : String =
    targetDirectory + fileOriginPath.diff(fileOriginDirectory)
}