package com.ktzsoft.FileManager

import java.io.{FileInputStream, ByteArrayInputStream}

import scala.collection.mutable
import scala.concurrent.Future

/**
 * Created by ktz on 15. 6. 9.
 */
trait FileManger [FileClass]{
  def ReadFile(fileToRead : FileClass): Future[FileInputStream]
  def WriteFile(ByteToWrite : Array[Byte], fileToWrite : FileClass) : Future[String]
  def DeleteFile(fileToDelete : FileClass) : Future[String]
  def CopyFile(fileFrom : FileClass, fileTo : FileClass) : Future[String]
  def GetFileList(filePath : String) : List[FileClass]
  def MoveFile(fileFrom : FileClass, fileTo : FileClass) : Future[String]
  def CheckIfSame(fileFrom : FileClass, fileTo : FileClass) : Future[Boolean]
}
