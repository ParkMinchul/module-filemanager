package com.ktzsoft.FileManager

import java.io.File

import akka.actor.{ActorLogging, Actor}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Success
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by ktz on 15. 6. 21.
 */

case class BackUpFiles()

class BackUpActor(mFileFrom : File, mFileTo : File) extends BackUpManager with Actor with ActorLogging{
  def receive = {
    case BackUpFiles() =>
      println("BackUp Files")
      val fFileList = CompareFiles(mFileFrom, mFileTo)
      val fileList = Await.result(fFileList, 10 second)
      fileList.foreach{ e =>
        if(e._2 == false) CopyFile(mFileFrom, mFileTo)
      }
      //ToDo Copy is Not Operate!!!!
  }
}
