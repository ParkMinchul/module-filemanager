package com.ktzsoft.FileManager

import java.io.{FileOutputStream, File}

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

import collection.mutable.Stack
import org.scalatest._

import scala.concurrent.{Future, Await}
import scala.concurrent.duration._
import scala.io.Source
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by ktz on 15. 6. 16.
 */
class FileManagerTest extends FlatSpec with Matchers{
  case class FileManagerTest() extends LocalFileManager

  "File Manager" should "Read File" in {
    val line = FileManagerTest().ReadFile(new File("test.mp4"))
    val fileIS = Await.result(line, 10 second)
  }

  it should "Copy File" in {
    var result = FileManagerTest().CopyFile(new File("test.pdf"), new File("test2.pdf"))
    Await.result(result, 20 second)
    result = FileManagerTest().DeleteFile(new File("test.pdf"))
    Await.result(result, 20 second)
    result = FileManagerTest().MoveFile(new File("test2.pdf"), new File("test.pdf"))
  }

  "Config Factory" should "Load Test" in {
    val config = ConfigFactory.load("application.conf")
    val hello = config.getAnyRef("file.name")
    hello.toString.toInt should be (3)
  }

  "BackUp Manager" should "Compare File" in {
    val system = ActorSystem("test")
    val manager = system.actorOf(Props(new BackUpActor(new File("/home/ktz/Documents/tmp"), new File("/home/ktz/Documents/hello"))))
    manager ! BackUpFiles()
  }
}